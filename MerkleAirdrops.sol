pragma solidity ^0.4.25;


contract MyTokenContract {
    
    mapping (address => uint256) public balanceOf;
    
    address owner;
    
    string public constant name = "Nikita coin";
    string public constant symbol = "NIK";
    uint256 public constant decimals = 18;
    uint256 public constant totalSupply = 12345678 * 10 ** 18;
    
    // function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
    // function transfer(address to, uint tokens) public returns (bool success);
    // function approve(address spender, uint tokens) public returns (bool success);
    // function transferFrom(address from, address to, uint tokens) public returns (bool success);
    
    event Transfer (address indexed from, address indexed to, uint256 value);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
    
    constructor () public payable {
       
        balanceOf[this] = totalSupply;
        owner = msg.sender;
    } 
    
  
    function () public payable {
        
        uint256 tokensPerEth = 1000;
        uint256 tokens = tokensPerEth*msg.value;

        require( balanceOf[this] >= tokens);
        
        balanceOf[this] -= tokens;
        balanceOf[msg.sender] += tokens;
        
        owner.transfer(msg.value);
        
        emit Transfer( this, msg.sender, tokens);
        
    }
    
    function transfer(address to, uint256 value) public returns (bool) {
        _transfer(msg.sender, to, value);
        return true;
    }

    function _transfer(address from, address to, uint256 value) internal {
        require(to != address(0));
        
        balanceOf[from] -= value;
        balanceOf[to] += value;
        emit Transfer(from, to, value);
    }
    
    function testHash(bytes32 h) public view returns(bytes32) {
       
       bytes32 output = keccak256(abi.encodePacked(h));
        return output;
        
    }
    
}

contract MerkleAirDrops {
    
    address public owner;
    bytes32 public merkleRoot;
    
    // Store addresses that already received tokens
    mapping (address => bool) recd;
    
    MyTokenContract tokenContract;
    event MerkleAidropTransfer(address _address, uint _amount);
    
    constructor( address _tokenContract, bytes32 _merkleRoot) {
        owner = msg.sender;
        merkleRoot = _merkleRoot;
        tokenContract = MyTokenContract(_tokenContract);
    }
    
    
    function setMerkleRoot( bytes32 _newMerkleRoot) public {
        require(msg.sender == owner);
        merkleRoot = _newMerkleRoot;
    }
    
    function checkProof( bytes32[] _merkleProof, bytes32 _hash) view internal returns(bool) {
        
        bytes32 hash = hash;
        bytes32 item;
        
        for (uint i = 0; i < _merkleProof.length; i++) {
        
            item = _merkleProof[i];
            
            if (hash < item) {
                hash = keccak256(hash, item);
            } else {
                hash = keccak256(item, hash);
            }
            
        }
        
        return hash == merkleRoot;
                
    }
    
    function getTokensByProof( bytes32[] _merkleProof, address _to, uint _amount) public returns(bool) {
        
        require(_amount > 0);
        require(!recd[_to]); 
        
        if (!checkProof( _merkleProof, keccak256(_to))) {
            return false;
        }
        
        if (tokenContract.transfer(_to, _amount)) {
            MerkleAidropTransfer(_to, _amount);
            return true;
        }
            
    }
    
    
}