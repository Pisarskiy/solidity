pragma solidity ^0.4.25;


library SafeMath {

  function mul(uint256 _a, uint256 _b) internal pure returns (uint256) {
   
    if (_a == 0) {
      return 0;
    }

    uint256 c = _a * _b;
    require(c / _a == _b);

    return c;
  }

  function div(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b > 0); // Solidity only automatically asserts when dividing by 0
    uint256 c = _a / _b;
    // assert(_a == _b * c + _a % _b); // There is no case in which this doesn't hold

    return c;
  }

  function sub(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b <= _a);
    uint256 c = _a - _b;

    return c;
  }

  function add(uint256 _a, uint256 _b) internal pure returns (uint256) {
    uint256 c = _a + _b;
    require(c >= _a);

    return c;
  }

  function mod(uint256 a, uint256 b) internal pure returns (uint256) {
    require(b != 0);
    return a % b;
  }
  
}


contract NikitaToken {
    
    using SafeMath for uint256;
    
    mapping (address => uint256) public balanceOf;
    
    address owner;
    
    string public constant name = "Nikita coin";
    string public constant symbol = "NIC";
    uint256 public constant decimals = 18;
    uint256 public constant totalSupply = 12345678 * 10 ** 18;
    
    // function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
    // function transfer(address to, uint tokens) public returns (bool success);
    // function approve(address spender, uint tokens) public returns (bool success);
    // function transferFrom(address from, address to, uint tokens) public returns (bool success);
    
    event Transfer (address indexed from, address indexed to, uint256 value);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
    
    constructor () public payable {
       
        balanceOf[this] = totalSupply;
        owner = msg.sender;
    } 
    
   
    function airDrops(uint tokens) public {
        
        require(msg.sender == owner);
        
        tokens = tokens*10**18;
        
        for (uint i=0; i<=uint(2000); i++) {
            sendTokens(address(0), tokens);
        }
        
    }
    
    
    function sendTokens( address _receiver, uint _amount) private {
        
        balanceOf[this] -= _amount;
        balanceOf[ _receiver] += _amount;
        
        emit Transfer( this, _receiver, _amount);
        
    }
    
    
    function () public payable {
        
        uint256 tokensPerEth = 1000;
        uint256 tokens = tokensPerEth.mul(msg.value);

        require( balanceOf[this] >= tokens);
        
        balanceOf[this] -= tokens;
        balanceOf[msg.sender] += tokens;
        
        owner.transfer(msg.value);
        
        emit Transfer( this, msg.sender, tokens);
        
    }
     
}