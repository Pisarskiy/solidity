pragma solidity ^0.4.25;

import "github.com/oraclize/ethereum-api/oraclizeAPI.sol";

library SafeMath {

  function mul(uint256 _a, uint256 _b) internal pure returns (uint256) {
   
    if (_a == 0) {
      return 0;
    }

    uint256 c = _a * _b;
    require(c / _a == _b);

    return c;
  }


  function div(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b > 0); // Solidity only automatically asserts when dividing by 0
    uint256 c = _a / _b;
    // assert(_a == _b * c + _a % _b); // There is no case in which this doesn't hold

    return c;
  }

  function sub(uint256 _a, uint256 _b) internal pure returns (uint256) {
    require(_b <= _a);
    uint256 c = _a - _b;

    return c;
  }

  function add(uint256 _a, uint256 _b) internal pure returns (uint256) {
    uint256 c = _a + _b;
    require(c >= _a);

    return c;
  }

  function mod(uint256 a, uint256 b) internal pure returns (uint256) {
    require(b != 0);
    return a % b;
  }
  
}


contract Wallet {
    
    Lottery public lottery;

    constructor() public {
        lottery = Lottery(msg.sender);
    }
    
    function() payable external {
    
        uint minBet = lottery.getMinBet();
        
        require(msg.value >= minBet);
      
        lottery.deposit.value(msg.value)(msg.sender);
        
    }
    
}

contract Lottery is usingOraclize {
   
    using SafeMath for uint;
    
    address owner;
   
    Wallet public ticket_0;
    Wallet public ticket_1;
    Wallet public ticket_2;
    
    enum lotteryStates {RELEASE,STARTED,STOPED}
    lotteryStates state;
   
    uint constant minBet = 0.1 ether;
    uint constant fee = 100;
    uint constant roundDuration = 86400;
    
    bytes32 public roundId;
    uint numPlayers;
    
    
    struct Ticket {
        address ticketAddress; // адрес контракта кошелька
        uint amountInvested; // баланс кошелька за день розыгрыша
    }
    
    mapping (address => Ticket) tickets; // Адрес кошелька - Ticket
   
   
    struct Player {
        address playerAddress;
        mapping( address => uint) amountByTicket;
        uint amountInvested;
    }
    
    mapping (address => mapping (bytes32 => uint)) public playerIdByAddress;
    mapping (uint => Player) players;
    uint[] public playersIDs;
    
    
    uint public jackpot;
    
    
    constructor() public {
        
        owner = msg.sender;
        
        ticket_0 = new Wallet();
        tickets[address(ticket_0)] = Ticket(address(ticket_0), 0);
        
        ticket_1 = new Wallet();
        tickets[address(ticket_1)] = Ticket(address(ticket_1), 0);
        
        ticket_2 = new Wallet();
        tickets[address(ticket_2)] = Ticket(address(ticket_2), 0);
    
        state = lotteryStates.RELEASE;
        
        numPlayers = 0;
        
    }
   
    modifier onlyOwner {
        require(msg.sender == owner);
        _;
     }
   
    // только адрес oraclize может вызвать функцию (используется в функции __callback)
    modifier onlyOraclize {
        require(msg.sender == oraclize_cbAddress());
        _;
    }
    
    modifier onlyStarded {
        require( state == lotteryStates.STARTED);
        _;
    }
    
    
    function getMinBet() public view returns(uint) {
        return minBet;
    }
    
    // Для вычисления доли одного числа к другому
    // for example, if you feed it 101, 450, 4 you get 2240, i.e. 22.4%.
    function percent(uint numerator, uint denominator, uint precision) internal constant returns(uint quotient) {

         // caution, check safe-to-multiply here
        uint _numerator  = numerator * 10 ** (precision+1);
        // with rounding of last digit
        uint _quotient =  ((_numerator / denominator) + 5) / 10;
        return ( _quotient);
    }
    
    
    // Функция запускает основную лотерею в действие
    function release() public payable onlyOwner {
        
        require(state == lotteryStates.RELEASE); // запрещаем запускать лотерею если она уже запущена

        state = lotteryStates.STARTED;
        
        // Запускаем новый раунд (день)
        // через roundDuration секунд oraclize в функции __callback вернет рандомное число от 1 до 3
        startNewRound(roundDuration);
        
    }
    
    // Функция регистрирует в лотерее нового участника
    function addPlayerAtID(address playerAddress, address ticketAddress, uint betValue)
        private {

        uint id;
        
        if (playerIdByAddress[playerAddress][roundId] == uint(0)) {
            
            // Это новый участник 
            numPlayers++;
            
            id = numPlayers;
            playerIdByAddress[playerAddress][roundId] = id;
            playersIDs.push(id);
            
            players[id].playerAddress = playerAddress;
            
        } else {
            
            // Участник с таким адресом уже делал ставки за день 
            id = playerIdByAddress[playerAddress][roundId];
        } 
        
        // обновляем информацию участника по суммам взносов
        players[id].amountByTicket[ticketAddress] += betValue;
        players[id].amountInvested += betValue; // 
        
    }
    
    
    // Эта функция вызывается, когда кто-то переводит эфир на один из трех кошельков
    function deposit(address _playerAddress) public payable 
        onlyStarded {
        
        uint betValue = msg.value;
        
        address _ticketAddress = msg.sender;
        require(msg.sender == tickets[_ticketAddress].ticketAddress); // Кошелек, выполнивший функцию не зарегестрирован в основной лотерее
        
        tickets[msg.sender].amountInvested += msg.value; // увеличиваем баланс кошелька
        
      
        // Добавляем/обновляем участника лотереи и его баланс 
        addPlayerAtID(_playerAddress, _ticketAddress, betValue);
        
        jackpot += betValue;
        
    }
    
    // Функция распределяет прибыль между участниками
    function profitDistribution(uint _winnerNumber) private {
        
        require(_winnerNumber >= 1 && _winnerNumber <= 3);
        
        // Меняем state, чтобы в момент распределения никто не делал ставки
        state = lotteryStates.STOPED;
        
        address winnerAddress;
        
        if (_winnerNumber == 1) {
            winnerAddress = address(ticket_0);        
        
        } else if (_winnerNumber == 2){
             winnerAddress = address(ticket_1);    
        
        } else if (_winnerNumber == 3){
             winnerAddress = address(ticket_2);     
        }
        
        
        uint ticketAmount = tickets[winnerAddress].amountInvested;
        
        uint amountInvested;
        address playerAddress;
        
        uint i;
        uint playersLength = playersIDs.length;
        
        uint amountToOwner = jackpot;
        
        if (ticketAmount <= 0) {
            
            // Никто не делал ставки на выигрышный Кошелек
            // Честно будет вернуть всем участникам их суммы (-1% комиссии)
            
            // Пробегаем по всем участникам и вычисляем кто сколько вносил
            // Отнимаем от суммы 1% комиссии и отправляем на адрес участника
            for (i = 0; i < playersLength; i++) {
               
                amountInvested = players[ playersIDs[i]].amountInvested;
                
                if (amountInvested > 0) {
                
                    amountInvested = amountInvested - (amountInvested.mul(fee).div(10000));
                    
                    playerAddress = players[ playersIDs[i]].playerAddress;
                    
                    if (amountInvested > 0) {
                        playerAddress.transfer(amountInvested);
                        amountToOwner -= amountInvested;
                    }
                
                }
                
            }
            
        } else {
            
            // Пробегаем по всем участникам и вычисляем тех, кто вкладывался в данный тикет
            // Вычисляем долю вклада участника в выигрышный контракт
            // Отнимаем 1% комиссии и отправляем выигрыш на адрес участника
            
            for (i = 0; i < playersLength; i++) {
                
                amountInvested = players[ playersIDs[i]].amountByTicket[winnerAddress];
                if (amountInvested > 0) {
                   
                    playerAddress = players[ playersIDs[i]].playerAddress;
                    
                    // Доля инвестора в выигрышный билет
                    uint playerPercentage = percent(amountInvested, ticketAmount, 4);
                    
                    // Сумма выигрыша без учета комиссии
                    uint profitAmount = jackpot.mul(playerPercentage).div(uint(10000));
                    
                    // Конечная сумма прибыли инвестора
                    profitAmount = profitAmount - (profitAmount.mul(fee).div(10000));
                    
                    if (profitAmount > 0) {
                        playerAddress.transfer(profitAmount);
                        amountToOwner -= profitAmount;
                    }
                
                }
            }      
            
        }
        
        // Обнуляем раунд
        jackpot = 0;
        
        delete playersIDs;
       
        tickets[address(ticket_0)].amountInvested = 0;
        tickets[address(ticket_1)].amountInvested = 0;
        tickets[address(ticket_2)].amountInvested = 0;
        
        if (this.balance >= amountToOwner) {
            owner.transfer(amountToOwner);
        }
        
        startNewRound(roundDuration);
        
    }

    
    // Функция запускает новый раунд (день) лотереи
    function startNewRound(uint scheduleTime) private {
       
        state = lotteryStates.STARTED;
        
        if (oraclize_getPrice("URL") > this.balance) {
            
            // На счету контракта не хватает средств чтобы сгенерировать новый запрос oraclize
            // приостанавливаем лотерею
            state = lotteryStates.STOPED;
    
        } else {
            // Рандомное число от 1 до 3, 
            // которое мы получим через scheduleTime секунд в функции __callback
            roundId = oraclize_query( scheduleTime, "URL", "json(https://api.random.org/json-rpc/1/invoke).result.random.data.0", '\n{"jsonrpc":"2.0","method":"generateIntegers","params":{"apiKey":"d025913b-ae24-47e0-bc13-0116d6c231d3","n":1,"min":1,"max":3,"replacement":true,"base":10},"id":1}');
        }    
       
    }
    
    // В эту функцию oraclizeAPI возвращает рандомный номер от 1 до 3
    function __callback(bytes32 myid, string result) public onlyOraclize {
      
        if (myid == roundId) {
            
            // Раунд закончен, распределяем прибыль 
            uint winnerNumber = parseInt(result);
            profitDistribution(winnerNumber);     
            
        }
        
    }
      
}
